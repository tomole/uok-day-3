package com.ocado.uok.robot;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;

import com.ocado.uok.robot.basket.Product;
import com.ocado.uok.robot.basket.ShoppingTask;
import com.ocado.uok.skilo.scanner.ScannerResult;

import java.util.ArrayList;
import java.util.Arrays;

import static com.ocado.uok.robot.R.layout;

/**
 * Ekran ze skanowaniem produktów.
 */
public class ScanActivity extends ActivityWithSomeMethods {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_scan);

        ScannerResult scannerResult =
                ViewModelProviders.of(this).get(ScannerResult.class);
        ShoppingTask shoppingTask =
                ViewModelProviders.of(this).get(ShoppingTask.class);

        ArrayList<Product> doZeskanowania = new ArrayList<>();
        doZeskanowania.add(Product.ONION);
        doZeskanowania.add(Product.ONION);
        doZeskanowania.add(Product.ONION);
        doZeskanowania.add(Product.APPLE);
        shoppingTask.setProductsToBuy(doZeskanowania);

        scannerResult.getLastScanned().observe(this, scannedText -> {
            int ileDoKupienia = 0;
            for(Product p: shoppingTask.getProductsToBuy()) {
                if(p == Product.of(scannedText)) {
                    ileDoKupienia++;
                }
            }

            int ileWKoszyku = 0;
            for(Product p: shoppingTask.getProductsInBasket()) {
                if(p == Product.of(scannedText)) {
                    ileWKoszyku++;
                }
            }
            if(ileWKoszyku < ileDoKupienia) {
                shoppingTask.addToBasket(Product.of(scannedText));
                showToast("Dodano do koszyka " + scannedText);
            } else {
                showToast(scannedText + " nie jest do zebrania");
            }

            scannerResult.getResumeCamera().call();
        });
    }

}
