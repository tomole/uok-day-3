package com.ocado.uok.robot;

import android.app.AlertDialog;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.ocado.uok.robot.time.Timer;

public class ActivityWithSomeMethods extends AppCompatActivity {

    private Timer timer = new Timer();

    public Timer getTimer() {
        return timer;
    }

    public void doWithDelay(long time, Runnable action) {
        // czekaj pół sekundy przed wznowieniem skanera
        new Handler().postDelayed(action, time);
    }

    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    public void showDialog(String title, String info, Runnable action) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(info)
                .setNeutralButton("OK", (d,w) -> action.run())
                .setCancelable(false)
                .create()
                .show();
    }
}
