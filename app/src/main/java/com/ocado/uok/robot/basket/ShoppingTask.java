package com.ocado.uok.robot.basket;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.ocado.uok.robot.time.TimeRecord;
import com.ocado.uok.robot.time.Timer;

import java.util.ArrayList;

/**
 * Model dla widoku z zadaniem skanowania.
 * Zawiera informacje jakie produkty należe zeskanować i jaki zostały już zeskanowane.
 * Obsługuje całą logikę związaną z koszykiem.
 */
public class ShoppingTask extends ViewModel {

    /**
     * Lista zebranych produktów w koszyku.
     * Opakowana w MutableLiveData, dzięki czemu można obserwować jej zmiany.
     */
    private final MutableLiveData<ArrayList<Product>> collectedProducts = new MutableLiveData<ArrayList<Product>>();

    // lista produktów, które należy zebrać
    private ArrayList<Product> toScanProducts = new ArrayList<>();

    public ShoppingTask() {
        collectedProducts.setValue(new ArrayList<>());
    }

    /**
     * Dodaj zeskanowany produkt do koszyka
     */
    public void addToBasket(Product product) {
        ArrayList<Product> actualCollected = collectedProducts.getValue();
        actualCollected.add(product);
        collectedProducts.postValue(actualCollected);
    }

    public ArrayList<Product> getProductsToBuy() {
        return toScanProducts;
    }

    /**
     * Ustaw listę produktów, jakie należy zeskanowania (wrzucić do koszyka)
     */
    public void setProductsToBuy(ArrayList<Product> products) {
        toScanProducts = products;
    }

    public MutableLiveData<ArrayList<Product>> getCollectedProducts() {
        return collectedProducts;
    }

    public ArrayList<Product> getProductsInBasket() {
        return collectedProducts.getValue();
    }

    /**
     * Sprawdź, czy koszyk zawiera wszystkie produkty do zeskanowania (jest kompletny)
     */
    public boolean isComplete() {
        return Product.firstContainsSecond(collectedProducts.getValue(), toScanProducts);
    }
}
